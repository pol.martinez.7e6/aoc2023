import * as fs from 'fs';
import * as readline from 'readline';

const filePath: string = './Day1.txt';

const fileStream = fs.createReadStream(filePath);
const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
});

let finalResult: number = 0;

rl.on('line', (line: string) => {
    let firstNum: number = 0;
    let secondNum: number = 0;
    let concatNumber: string;

    for (let c = 0; c < line.length; c++) {
        const char = line[c];
        if (parseInt(char) >= 0 && parseInt(char) <= 9) {
            firstNum = parseInt(char);
            break
        }
    }

    for (let c = line.length; c >= 0; c--) {
        const char = line[c];
        if (parseInt(char) >= 0 && parseInt(char) <= 9) {
            secondNum = parseInt(char);
            break
        }
    }

    concatNumber = firstNum.toString() + secondNum.toString();
    finalResult += parseInt(concatNumber);
});

rl.on('close', () => {
    console.log(finalResult);
});